var express = require('express')
var app = express()
var cors = require('cors')
var bodyParser = require('body-parser')
var oracledb = require('oracledb');
var Q = require('q');

var assetService = require('./asset_table_service.js');

var peopleService = require('./people_service.js');

var tagsService = require('./tags_table_service.js');
var peopleTableService = require('./people_table_service.js');

app.use(bodyParser.json());
app.use(cors())

var sworm = require('sworm');

var ORACLE_DB_CONFIG = {
    driver: 'oracle',
    config: {
        user: 'student',
        password: 'STUDENT',
        connectString: 'localhost:49161/XE'
    }
};

var db = sworm.db(ORACLE_DB_CONFIG);
db = assetService.getDBCon();

app.get('/', function (req, res) {
    res.send('Hello World!')
})

app.get('/db', function (req, res, next) {
    db.query('select * from users').then(function (data) {
        res.send(data);
    }, function (err) {
        console.log('select - ' + err);
    });
})

app.get('/db/:userId', function (req, res, next) {
    var userId = req.params.userId;
    db.query("select FAMILYTREE.get_user_id('Taticut') from dual").then(function (data) {
        console.log(data);
        res.json({'user': data});
        throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

app.get('/questions/:questionId', function (req, res, next) {
    var questionId = req.params.questionId;
    db.query("select dbms_lob.substr(question, 4000, 1) as question, dbms_lob.substr(answer, 4000, 1) as answer from questions where id = " + sworm.escape(questionId)).then(function (data) {
        console.log(data);
        console.log('safe');
        console.log(questionId);
        console.log(sworm.escape(questionId));
        console.log(req.params);
        res.json({'question': data});
    }, function (err) {
        console.log('select - ' + err);
        res.set('Content-Type', 'application/json');
        res.status(500).send(JSON.stringify({
            status: 500,
            message: "Student not found",
            detailed_message: err.message
        }))

    });
})

app.get('/questions/unsafe/:questionId', function(req, res, next) {
  var questionId = req.params.questionId;

  console.log('unsafe');
  console.log(questionId);
  console.log(sworm.unescape(questionId));
  console.log(req.params);
  db.query('select dbms_lob.substr(question, 4000, 1) as question, dbms_lob.substr(answer, 4000, 1) as answer from questions where id = @questionId',
    {
      questionId: sworm.unescape(questionId)
    }).then(function(data) {
    console.log(data);
    res.json({'question': data});
  }, function(err) {
    console.log('select - ' + err);
    res.send(err);
  });
})

app.post('/asset/video', function(req, res, next) {
var assetList = JSON.parse(req.body.assets);
var defaultSelectQuery = "insert into videos values ";

assetList.forEach(function(currentAsset) {
console.log(currentAsset);
var currentAssetQuery = JSON.parse(JSON.stringify(defaultSelectQuery)) + "(";
currentAssetQuery += 0 + ",";
for (key in currentAsset) {
  var fieldValue;
  //key == "my_users_id" ? fieldValue = currentAsset[key] : fieldValue = "'" + (currentAsset[key] ? currentAsset[key] : 'NULL') + "'";
  //console.log(currentAsset["my_users_id"]);
  fieldValue = "'" + (currentAsset[key] ? currentAsset[key] : 'NULL') + "'";
  if(key == "posting_date") {
    fieldValue = "to_date(" + "'"+currentAsset[key]+"'" + ",'DD/MM/YYYY/ HH:MI:SS')";
  }
  currentAssetQuery += fieldValue + ",";
}

currentAssetQuery = currentAssetQuery + "0)";
currentAssetQuery = currentAssetQuery.replace(/'NULL'/gi, 'NULL');
console.log(currentAssetQuery);

db.statement(currentAssetQuery).then(function(data) {
  res.send(data);
}, function(err) {
  console.log('video insert - ' + err);
  res.send(err);
});
})
})


app.post('/db', function (req, res, next) {
    var user = db.model({'table': 'users'})

    var userObject = req.body.user[0];

    delete userObject.created_at;
    delete userObject.updated_at;

    for (key in req.body) {
        if (key !== 'user') {
            userObject[key] = req.body[key];
        }
    }

    console.log('userObject - ', userObject);
    var userToUpdate = user(userObject);

    userToUpdate.upsert().then(function (response) {
        console.log('update response - ', response);
        res.send('Update successfull');
    }, function (err) {
        console.log('update err - ', err);
        res.send(err);
    });


    console.log(req.params);
    console.log(req.body);
    console.log('save - ' + req.body.name);
})

app.delete('/db/:userId', function (req, res, next) {
    console.log(req.params.userId);
    console.log(req.body);
    console.log('delete - ' + req.body.name);

    oracledb.getConnection(ORACLE_DB_CONFIG.config, function (err, connection) {
        if (err) {
            res.set('Content-Type', 'application/json');
            res.status(500).send(JSON.stringify({
                status: 500,
                message: "Error connecting to DB",
                detailed_message: err.message
            }))
            return;
        }

        connection.execute('DELETE FROM REAL_USERS WHERE REAL_USER_ID = :userId', [req.params.userId], {
            isAutoCommit: true,
            outFormat: oracledb.OBJECT
        }, function (err, result) {
            console.log('err - ' + err);
            console.log('result - ' + result);
            res.sendStatus(200);

            connection.release(function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('DELETE <RESOURCE>: CONNECTION RELEASED');
                }
            })
        });
    })
})

app.set('/db', function (req, res, next) {
    console.log(req);
});


// USERS

app.get('/users', function (req, res, next) {
    db.query('select * from my_users').then(function (data) {
        res.send(data);
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

app.get('/users/count', function (req, res, next) {
    db.query('select count(1) from my_users').then(function (data) {
        res.send(data);
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

// - PAGINATION
app.get('/users/:page/:pageSize', function (req, res, next) {
    var userId = req.params.userId;
    req.params.page = parseInt(req.params.page, 10);
    req.params.pageSize = parseInt(req.params.pageSize, 10);

    db.query("SELECT * FROM (SELECT rownum rn, user_id,first_name,last_name,birthday, telephone, email ,username, hash FROM (SELECT * FROM my_users ORDER BY user_id) WHERE rownum <=" + (req.params.page + 1) * req.params.pageSize + ") WHERE rn >= " + req.params.page * req.params.pageSize + "").then(function (data) {
        res.send(data);
        throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

// - PAGINATION + FILTER
app.get('/users/:page/:pageSize/:params', function (req, res, next) {
    console.log('inside filter');
    var userId = req.params.userId;
    req.params.page = parseInt(req.params.page, 10);
    req.params.pageSize = parseInt(req.params.pageSize, 10);

    console.log(req.params);

    var whereClause = '';
    var parsedParams = JSON.parse(req.params.params);
    if (parsedParams.birthday) {
        parsedParams.birthday = "to_date(" + "'" + parsedParams.birthday.split('T')[0] + "'" + ",'YYYY/MM/DD')";
    }

    for (var key in parsedParams) {
        if (parsedParams.hasOwnProperty(key)) {
            console.log(key + '-' + parsedParams[key]);
            if (key == 'birthday') {
                whereClause = whereClause + " AND " + key + " = " + parsedParams[key];
            } else {
                whereClause = whereClause + " AND " + key + " = " + "'" + parsedParams[key] + "'";
            }
        }
    }

    whereClause = whereClause.replace(/@/g, "'||utl_url.unescape('%40')||'");
    console.log(whereClause);

    db.query("SELECT * FROM (SELECT rownum rn, user_id,first_name,last_name,birthday, telephone, email ,username, hash FROM (SELECT * FROM my_users  where 1=1" + whereClause + " ORDER BY user_id) WHERE rownum <=" + (req.params.page + 1) * req.params.pageSize + ") WHERE rn >= " + req.params.page * req.params.pageSize + "").then(function (data) {
        res.send(data);
        throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.status(500).send({error: err});
    });
})

// TREE
app.get('/familyTree/userId/:obj', function (req, res, next) {
    console.log('-------------------------------');
    var my_users_id=JSON.parse(req.params.obj).my_users_id;
    console.log("select * from treetable where user_id = '"+my_users_id+"'");
    db.query("select * from treetable where user_id = '"+my_users_id+"'").then(function (data) {
        res.send(data);
    }, function (err) {
        console.log('select - ' + err);
    });
})

app.get('/familyTree/getPath/:root/:source', function (req, res, next) {
    db.query("SELECT FAMILYTREE.find_path('" + req.params.root + "', '" + req.params.source + "') FROM dual").then(function (data) {
        res.send(data);
        throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

app.get('/familyTree/children/:root/', function (req, res, next) {
    console.log(req.params.root);
    db.query("SELECT FAMILYTREE.get_all_children('" + req.params.root + "', 1) FROM dual").then(function (data) {
        console.log(data);
        res.send(data);
        throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

app.get('/familyTree/update/:node/', function (req, res, next) {
    console.log(req.params.node);
    req.params.node = JSON.parse(req.params.node);
    db.statement("update treetable set nickname ='" + req.params.node.newNickname + "' where person_id = " + parseInt(req.params.node.person_id)).then(function (data) {
        console.log(data);
        res.send(data);
        throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

app.delete('/familyTree/:userId', function (req, res, next) {
    console.log(req.params.userId);
    console.log(req.body);
    console.log('delete - ' + req.body.name);

    oracledb.getConnection(ORACLE_DB_CONFIG.config, function (err, connection) {
        if (err) {
            res.set('Content-Type', 'application/json');
            res.status(500).send(JSON.stringify({
                status: 500,
                message: "Error connecting to DB",
                detailed_message: err.message
            }))
            return;
        }

        connection.execute('DELETE FROM TREETABLE WHERE PERSON_ID = :userId', [req.params.userId], {
            isAutoCommit: true,
            outFormat: oracledb.OBJECT
        }, function (err, result) {
            console.log('err - ' + err);
            console.log('result - ' + result);
            res.sendStatus(200);

            connection.release(function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('DELETE <RESOURCE>: CONNECTION RELEASED');
                }
            })
        });
    })
})

app.post('/familyTree', function (req, res, next) {
    var user = db.model({'table': 'treetable'})

    var userObject = req.body.user[0];

    for (key in req.body) {
        if (key !== 'user') {
            userObject[key] = req.body[key];
        }
    }

    console.log('userObject - ', userObject);
    var userToUpdate = user(userObject);

    console.log(user);
    console.log(userObject);
    userToUpdate.upsert().then(function (response) {
        console.log('update response - ', response);
        res.send('Update successfull');
    }, function (err) {
        console.log('update err - ', err);
        res.send(err);
    });
})

app.get('/familyTree/:userId', function (req, res, next) {
    var userId = req.params.userId;
    db.query("select * from treetable where person_id = " + userId).then(function (data) {
        console.log(data);
        res.json({'user': data});
        throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.send(err);
    });
})

app.get('/familyTree/add/:node/userId/:userId', function (req, res, next) {
    console.log("//////////////////////////////////");
    var parsedNode = JSON.parse(req.params.node);
    var userId=req.params.userId;
    console.log('parsed node    '+JSON.stringify(parsedNode));
    console.log(parsedNode.kinship);
    //var queryString="begin familytree.add_person(1,'"+parsedNode.nickname+"' , '"+parsedNode.kinship+"', 'Taticut', 0); end;";
    //console.log('[queryString]'+queryString);

    console.log("begin familytree.add_person('"+parsedNode.my_users_id+"','" + parsedNode.nickname + "' , '" + convertComplexKinship(JSON.stringify(parsedNode.kinship)) + "', '" + parsedNode.root + "', 0); end;");
    db.statement("begin familytree.add_person('"+parsedNode.my_users_id+"','" + parsedNode.nickname + "' , '" + convertComplexKinship(JSON.stringify(parsedNode.kinship)) + "', '" + parsedNode.root + "', 0); end;").then(function (data) {
        console.log(data);
        res.json({'user': data});
        //throw new Error('some rror');
    }, function (err) {
        console.log('select - ' + err);
        res.set('Content-Type', 'application/json');
        res.status(500).send(JSON.stringify({
            status: 500,
            message: "Error ading stuff",
            detailed_message: err.message
        }))
    });
})

app.get('/assets/count/:userId', function (req, res, next) {
    var user = req.params.userId;
    console.log("Count asset for user " + user);
    assetService.countAllAssets(user).then(function (result) {
        console.log("Counted " + result.message);
        res.status(200).send(result);
    });
})

// app.get('/assets/page/:userId/:page/:pageSize', function (req, res, next) {
//     var user = req.params.userId;
//     var page = req.params.page;
//     var pageSize = req.params.pageSize;
//     console.log("Paginated asset for user " + user + " page " + page + " pagesize " + pageSize);
//     assetService.getPaginatedAssets(user, page, pageSize).then(function (result) {
//         console.log("Paginated res " + result);
//         res.status(200).send(result);
//     });
// })

app.get('/insertTag/:tagDetails', function (req, res, next) {
    console.log('inside');
    var tagDetails = JSON.parse(req.params.tagDetails);

    tagsService.insertTagComplete(tagDetails.tag_name, tagDetails.my_users_id, 1, tagDetails.file_type).then(function (data) {
        console.log('ok');
        res.status(200).send('OK');
    }, function (err) {
        res.status(500).send(err);
    });

})

app.post('/tags/:tagArray', function (req, res, next) {
    //mind what should the tag item include
    //console.log(JSON.stringify(JSON.parse(req.params.tagArray)));
    var tagArray = JSON.parse(req.params.tagArray);
    console.log("inserting " + JSON.stringify(tagArray));
    tagsService.insertTagsArray(tagArray).then(function (data) {
        res.status(200).send('OK');
    }, function (err) {
        res.status(500).send(err);
    })

})

app.post('/people/:peopleArray',function(req,res,next){
    var peopleArray=JSON.parse(req.params.peopleArray);
    peopleTableService.insertPeopleArray(peopleArray).then(function(response){
        console.log('       [succes adding peeople array ');
    },function(err){
        console.log('       error people array'+err);
    })


})

app.put('/tags/:tagArray', function (req, res, next) {
    //update
    var tagArray = JSON.parse(req.params.tagArray);
    tagsService.updateTagsArray(tagArray).then(function (data) {
        res.status(200).send('OK');
    }, function (err) {
        res.status(500).send(err);
    })

})


app.post('/addAssets/:assets', function (req, res, next) {
    var assets = JSON.parse(req.params.assets);
    console.log('Adding assets: ' + req.params.assets);
    var result = assetService.addAssets(assets).then(function (result) {
        console.log("returned");
        console.log(result);
        res.status(result.status).send(result.message);
    });
})

app.put('/assets/:asset', function(req, res, params) {
  var asset = req.params.asset;
  console.log("updating " + asset);
  assetService.updateAsset(JSON.parse(asset)).then(function(data){
    console.log("returned " + JSON.stringify(data));
    res.send(data);
  })

})

app.put('/assets/delete/:asset', function(req, res, params) {
  var asset = req.params.asset;
  console.log("updating " + asset);
  assetService.removeAsset(JSON.parse(asset)).then(function(data){
    console.log("returned " + JSON.stringify(data));
    res.send(data);
  })

})

app.get('/getPeopleId/:nickname/:my_users_id', function (req, res, next) {

    var nickname = JSON.stringify(req.params.nickname);
    var my_users_id = req.params.my_users_id;

    peopleTableService.getPeopleId(nickname, my_users_id).then(function (data) {
        res.status(200).send(data);
    }, function (err) {
        res.status(500).send(err);
    })
})


app.get('/assetId/:creation_date/:file_type',function(req,res,next) {
    console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
    var creation_date = req.params.creation_date;
    var file_type = req.params.file_type;
    tagsService.getAssetId(creation_date, file_type).then(function (response) {
        console.log('[assetId] - succes');
        res.status(200).send(JSON.stringify(response));
    }, function (err) {
        console.log('[assetId] - error ' + err);
        res.status(500).send(err);
    })
})

    app.get('/assets/page/:userId/:page/:pageSize', function (req, res, next) {
        var user = req.params.userId;
        var page = parseInt(req.params.page);
        var pageSize = parseInt(req.params.pageSize);
        console.log("Paginated asset for user " + user + " page " + page + " pagesize " + pageSize);
        assetService.getCompleteAsset(user, page, pageSize)
            .then(function (result) {
                console.log('====================================');
                console.log(result);
                console.log('====================================');
                res.send(result);
            })

        // assetService.getPaginatedAssets(user, page, pageSize).then(function(result){
        //   var promises = [];
        //   for(var index in result){
        //     var asset = result[index];
        //     delete asset.rn;
        //     console.log(asset);
        //     promises.push(Q.promise(function(resolve, reject){
        //       var ind = index;
        //         peopleService.getPeople(user, asset.asset_id, asset.file_type)
        //             .then(function(data) {
        //               result[ind].persons  = data;
        //               console.log("got people");
        //               if(data.length!=0){
        //                 console.log("asset with people");
        //                 console.log(asset);
        //               }
        //               resolve(data);
        //             }, function(err){
        //               console.log('get people err ' + JSON.stringify(err));
        //               reject(err);
        //             })
        //     }))
        //   }
        //   Q.all(promises).then(function(data){
        //     console.log('sending');
        //     res.send(result);
        //   })
        // });
    })

    app.get('/assets/complexSearch/:userId/:obj', function (req, res, next) {
        assetService.complexSearch(req.params.userId, JSON.parse(req.params.obj))
            .then(function (data) {
                console.log(typeof data);
                res.send(data);
            })
    })

    app.post('/addAssets/:assets', function (req, res, next) {
        var assets = JSON.parse(req.params.assets);
        console.log('Adding assets: ' + req.params.assets);
        var result = assetService.addAssets(assets).then(function (result) {
            console.log("returned");
            console.log(result);
            res.status(result.status).send(result.message);
        });
    })

    app.get('/addPersonIntoPersonsOnly/:nickname/:my_users_id', function (req, res, next) {
        //BE CAREFUL WHEN USING ! WILL NOT ADD INTO FOREIGN KEY TABLE!!!!
        var nickname = req.params.nickname;
        var my_users_id = req.params.my_users_id;
        peopleTableService.insertPersonsIntoPersonsOnly(nickname, my_users_id).then(function () {
            res.status(200).send('OK');
        }, function (err) {
            console.log(JSON.stringify(err));
            res.status(500).send(err);
        })
    })


    app.get('/insertPeopleComplete/:nickname/:assetId/:my_users_id/:file_type', function (req, res, next) {
        res.status(200).send("OK");
    }, function (err) {
        res.status(500).send(err);

    })

// app.get('/insertPeopleComplete/:nickname/:assetId/:my_users_id/:file_type').then(function (data) {
//     res.status(200).send("OK");
// }, function (err) {
//     res.status(500).send(err);
// })

    app.listen(3005, function () {
        console.log('Example app listening on port 3005!')
    })

// Tree Related Functions

    function convertComplexKinship(inputKinship) {
        console.log('input'+inputKinship);
        if (typeof(inputKinship) == "string") {
            var basicKinships = {
                "kid": "S",
                "mother": "F",
                "father": "F",
                "brother": "FS",
                "sister": "FS",
                "grandfather": "FF",
                "grandmother": "FF",
                "uncle": "FFS",
                "aunt": "FFS",
                "great-grandfather": "FFF",
                "great-grandmother": "FFF",
                "hector" : "W"
            };
            //var parsedArray = JSON.stringify(inputKinship.split(' ').reverse());
            var parsedArray=String(inputKinship).split(' ').reverse();
            console.log('parsed: '+JSON.stringify(parsedArray));
            var finalArray = [];
            console.log(basicKinships);

            for (var kinshipElement in parsedArray) {
                //console.log('el'+parsedArray[kinshipElement])
                var str=parsedArray[kinshipElement].replace(/\"/gi,"");
                var hec="kid";
                console.log("Str: "+str);
                console.log("Hec: "+hec);
                console.log('basic'+basicKinships[str]);
                console.log('basic'+basicKinships[hec]);
                finalArray.push(basicKinships[str]);
                //console.log(basicKinships[parsedArray[kinshipElement]]);
                console.log(basicKinships["kid"]);
            }
            console.log('array: '+JSON.stringify(finalArray));
            var finalString = finalArray.toString().replace(/,/g, '');
            console.log('final: '+finalString)
            finalString = normalizeKinshipPath(finalString);
            console.log('final: '+finalString)
            finalString = finalString.toString().replace(/,/g, '');
            console.log('final: '+finalString)

            if (finalString[finalString.length - 1] == 'S') {
                finalString = finalString.replace(/S$/g, 'N');
                console.log('final: '+finalString)


            }

            console.log(finalString);
            return finalString;
        }
        else {
            console.log('bad param.');
            return 'bad param';
        }
    }

    function normalizeKinshipPath(inputKinshipPath) {

        var splittedKinshipArray = inputKinshipPath.split('');
        var finalArray = [];
        var kinshipElement = 0;

        for (kinshipElement = 0; kinshipElement < splittedKinshipArray.length; kinshipElement++) {
            if (splittedKinshipArray[kinshipElement] == 'S' && splittedKinshipArray[kinshipElement + 1] == 'F') {
                kinshipElement = kinshipElement + 1;
            }
            else {
                finalArray.push(splittedKinshipArray[kinshipElement]);
            }

        }
        //console.log(finalArray);
        return finalArray;

    }
//================
//
// function convertComplexKinship(inputKinship) {
//     if (typeof(inputKinship) == "string") {
//         var basicKinships = {
//             "kid": "S",
//             "mother": "F",
//             "father": "F",
//             "brother": "FS",
//             "sister": "FS",
//             "grandfather": "FF",
//             "grandmother": "FF",
//             "uncle": "FFS",
//             "aunt": "FFS",
//             "great-grandfather": "FFF",
//             "great-grandmother": "FFF"
//         };
//         var parsedArray = inputKinship.split(' ').reverse();
//         var finalArray = [];
//
//         for (var kinshipElement in parsedArray) {
//             finalArray.push(basicKinships[parsedArray[kinshipElement]]);
//         }
//
//         var finalString = finalArray.toString().replace(/,/g, '');
//         finalString = normalizeKinshipPath(finalString);
//         finalString = finalString.toString().replace(/,/g, '');
//
//         if (finalString[finalString.length - 1] == 'S') {
//             finalString = finalString.replace(/S$/g, 'N');
//         }
//         console.log(finalString);
//         return finalString;
//     }
//     else {
//         console.log('bad param.');
//         return 'bad param';
//     }
// }
//
// function normalizeKinshipPath(inputKinshipPath) {
//
//     var splittedKinshipArray = inputKinshipPath.split('');
//     var finalArray = [];
//     var kinshipElement = 0;
//
//     for (kinshipElement = 0; kinshipElement < splittedKinshipArray.length; kinshipElement++) {
//         if (splittedKinshipArray[kinshipElement] == 'S' && splittedKinshipArray[kinshipElement + 1] == 'F') {
//             kinshipElement = kinshipElement + 1;
//         }
//         else {
//             finalArray.push(splittedKinshipArray[kinshipElement]);
//         }
//
//     }
//     //console.log(finalArray);
//     return finalArray;
//
// }

//==================




//convertComplexKinship('unchi bunica');
//convertComplexKinship('kid kid');
//normalizeKinshipPath("FFSFSFSFF");
