/**
 * Created by RobertHerscovici on 5/28/2017.
 */
var tagsTable = module.exports = {};
var Q = require('q');
var database = require('./db.js');
var db = database.getConnection();

// tagsTable.addTags = function (tagArray) {
//     console.log('Inside addTags');
//     console.log('[tagarray]' + JSON.stringify(tagArray));
//     var promiseArray
//     console.log('[size]' + tagArray.length);
//     if (tagArray.length > 0 && tagArray != undefined) {
//         for (var i in tagArray) {
//             var tag = tagArray[i];
//             console.log('       [tag]' + JSON.stringify(tag));
//             //TODO de schimbat pe primul insert stmt comentat ca sa se adauge my_user_id
//             //var insertstmt = "INSERT INTO TAGS VALUES (1, '"+tag.name+ "' , " +  Number(tag.my_user_id) + ",0 )";
//             var insertstmt = "INSERT INTO TAGS(NAME,MY_USERS_ID,IS_DELETED) VALUES ('"+tag.name+ "' , " +  Number(tag.my_user_id) + ",0 )";
//
//             db.statement(insertstmt).then(function (data) {
//                 console.log('[succes]');
//             }, function (err) {
//                 console.log('[reject]'+err);
//             })
//
//         }
//
//     }
// }

tagsTable.getTagId = function (tag_name, user_id) {
    return new Promise(function (resolve, reject) {
        var selectstmt = "SELECT TAGS_ID FROM TAGS WHERE MY_USERS_ID = '" + user_id + "' and UPPER(name) like '%" + tag_name.toUpperCase() + "%'";
        console.log(selectstmt);
        db.query(selectstmt).then(function (data) {
            console.log('[data]' + data);
            resolve(data[0]);
        }, function (err) {
            console.log('error - ' + err);
            reject(err);
        });

    })

}

tagsTable.insertTagIntoTagsOnly=function(name, my_user_id) {
  return new Promise(function (resolve, reject) {
      var insertstmt = "INSERT INTO TAGS(NAME,MY_USERS_ID,IS_DELETED) VALUES ('" + name + "' , '" + my_user_id + "',0 )";
      console.log(insertstmt);
      db.statement(insertstmt).then(function (response) {
          console.log('[insertTag] succes' + JSON.stringify(response));
          resolve();
      }, function (err) {
          console.log('[insertTag] failed' + err);
          reject(err);
      })
  })
}

tagsTable.insertTagComplete = function (tag_name, my_users_id, asset_id, asset_file_type) {
    console.log('assetId' + asset_id);
    return new Promise(function (resolve, reject) {
        tagsTable.insertTagIntoTagsOnly(tag_name, my_users_id).then(function (response) {
            tagsTable.getTagId(tag_name, my_users_id).then(function (rsp) {
                var insertKeyForeign = "INSERT INTO " + asset_file_type + "_TAGS VALUES (10," + asset_id + ", " + Number(rsp.tags_id) + ",0)";
                console.log('insertKeyForeign ' + insertKeyForeign);
                db.statement(insertKeyForeign).then(function (data) {
                    console.log('[succes]' + JSON.stringify(data));
                    resolve();
                }, function (err) {
                    console.log('[failure]' + err);
                    reject("Problems in insertin into foreign key table" + err);
                })
            }, function (er) {
                reject("Problems getting the tag id " + err);

            })
        }, function (err) {
            reject("Problems inserting the tag" + err);

        })
    })
}


updateTag = function(tag_name, my_users_id, asset_id, asset_file_type, is_deleted) {
    console.log('assetId '+parseInt(asset_id));

    return new Promise(function (resolve, reject) {
            tagsTable.getTagId(tag_name, my_users_id).then(function (rsp) {

              console.log(rsp);
                var insertKeyForeign = "UPDATE TAGS SET name='" + tag_name + "', is_deleted="+parseInt(is_deleted) + " WHERE tags_id=" + Number(rsp.tags_id);
                db.statement(insertKeyForeign).then(function (data) {
                    console.log('[succes]' + JSON.stringify(data));
                    resolve();
                }, function (err) {
                    console.log('[failure]' + err);
                    reject("Problems in updating tags" + err);
                })
            }, function (er) {
                reject("Problems updating the tag " + err);

            })
        }, function (err) {
            reject("Problems updating the tag" + err);

        })
}

tagsTable.getAssetId = function (creation_date, file_type) {
    return new Promise(function (resolve, reject) {
        console.log(creation_date + '            ' + file_type);
        var selectStmt = "SELECT " + file_type + "_ID AS ASSET_ID FROM " + file_type + " WHERE creation_date = TO_TIMESTAMP( '" + creation_date
            + "',\'DD-MM-YYYY hh:MI:ss\')";

        selectStmt = selectStmt.replace(/&/g, "'||utl_url.unescape('%26')||'");
        console.log('[selectstmt]' + selectStmt);
        db.query(selectStmt).then(function (data) {
            console.log('[data]' + JSON.stringify(data[0]));
            resolve(data[0].asset_id);
        }, function (err) {
            console.log('select - ' + err);
            reject(err);
        });
    });
}

tagsTable.insertTagsArray = function (tagArray) {
    console.log('inside');
    return new Promise(function (resolve, reject) {
        var promiseArray = [];
        for (var i in tagArray) {
            console.log(JSON.stringify('tag in array' + tagArray[i]));
            if (tagArray[i].asset_id != undefined) {
                promiseArray.push(tagsTable.insertTagComplete(tagArray[i].tag_name, tagArray[i].my_users_id, tagArray[i].asset_id, tagArray[i].file_type));
            }
        }
        Promise.all(promiseArray).then(function (data) {
            resolve("OK");
        }, function (err) {
            reject(err);
        })
    })
}

tagsTable.updateTagsArray = function(tagArray) {
  console.log('updating');
  return new Promise(function (resolve, reject) {
      var promiseArray = [];
      for (var i in tagArray) {
          console.log('tag in array '+JSON.stringify(tagArray[i]));
          promiseArray.push(updateTag(tagArray[i].tag_name, tagArray[i].my_users_id, tagArray[i].asset_id, tagArray[i].file_type, tagArray[i].is_deleted));
      }
      Promise.all(promiseArray).then(function (data) {
          resolve("OK");
      }, function (err) {
          reject(err);
      })
  })

}

function getAssetId(creation_date, file_type) {
    return new Promise(function (resolve, reject) {
        var selectStmt = "SELECT " + file_type + "_ID AS ASSET_ID FROM " + file_type + " WHERE creation_date = TO_TIMESTAMP( '" + creation_date
            + "',\'DD-MM-YYYY hh:MI:ss\')";

        selectStmt = selectStmt.replace(/&/g, "'||utl_url.unescape('%26')||'");
        console.log('[selectstmt]' + selectStmt);
        db.query(selectStmt).then(function (data) {
            console.log('[data]' + JSON.stringify(data[0]));
            resolve(data);
        }, function (err) {
            console.log('select - ' + err);
            reject(data[0]);
        });
    })
}

