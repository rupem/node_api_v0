var sworm = require('sworm');

var ORACLE_DB_CONFIG = {
  driver: 'oracle',
  config: {
    user: 'student',
    password: 'STUDENT',
    connectString: 'localhost/XE',
    pool: true,

    options: {
      // options to set on `oracledb`
      maxRows: 1000
    }
  }
};


var service = {
  getUsers: getUsers
};
var db = sworm.db(ORACLE_DB_CONFIG);

function getUsers() {
  return db.query('select * from users');
}

console.log(getUsers().then(function(res) {return res;}));
module.exports = getUsers;
