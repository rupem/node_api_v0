var moment = require('moment');
var assetTable = module.exports = {};
var Q = require('q');
var database = require('./db.js');
var db = database.getConnection();

var tables = ["PDFS", "DOCS", "VIDEOS", "PHOTOS"];


function chooseTable(type) {
  if (type == undefined) {
    console.log("DOCS");
    return "docs";
  }
  if (type.match(/pdf/i)) {
    console.log("PDFS");
    return "pdfs";
  }
  if (type.match(/avi/i)) {
    console.log("VIDEOS");
    return "videos";
  }
  if (type.match(/jpg/i) || type.match(/png/i)) {
    console.log("PHOTOS");
    return "photos";
  }
  console.log("DOCS");//docx; xlsx
  return "docs";
}

function checkForNULLValues(asset) {
  if (asset == undefined || asset.length == 0) {
    return null;
  } else {
    return '\'' + asset + '\'';
  }
}

assetTable.addAssets = function (assets) {
  return Q.promise(function (resolve, reject) {
    var promises = [];
    var changesDone = 0;
    for (var ind in assets) {
      var asset = assets[ind];
      var table = chooseTable(asset.file_type);
      var type = table.slice(0, -1);

      var insertStmt = "INSERT INTO " + table + " VALUES(0,'"
        + asset.my_users_id + "', "
        + checkForNULLValues(asset.title) + ', '
        + 'TO_TIMESTAMP('
        + checkForNULLValues(asset.posting_date)
        + ',\'DD-MM-YYYY hh:MI:ss\'), TO_TIMESTAMP('
        + checkForNULLValues(asset.creation_date)
        + ',\'DD-MM-YYYY hh:MI:ss\'), '
        + checkForNULLValues(asset.location) + ', '
        + checkForNULLValues(asset.description) + ', \''
        + type + '\', '
        + checkForNULLValues(asset.asset_path) + ', '
        + checkForNULLValues(asset.preview_src_link)
        + ",0)";
      insertStmt = insertStmt.replace(/&/g, "'||utl_url.unescape('%26')||'");
      console.log(insertStmt);
      promises.push(
        Q.promise(function (resol, rej) {
          db.statement(insertStmt)
            .then(function (data) {
              changesDone = changesDone + data.changes;
              resol(changesDone);
            }, function (err) {
              console.log('Error insert asset - ' + err);
              rej(err);
            });
        })
      )

    }
    Q.all(promises).then(function (result) {
      console.log('got result ' + result);
      var resp = {status: 200, message: {changes: changesDone}};
      resolve(resp);
    });
  })

}

assetTable.countAllAssets = function (userId) {
  return Q.promise(function (resolve, reject) {
    var assets = 0;
    var promises = [];
    for (key in tables) {
      var table = tables[key];
      var stmt = "SELECT COUNT(1) as res FROM " + table + " WHERE my_users_id = '" + userId + "' AND is_deleted=0";
      console.log(stmt);
      promises.push(
        Q.promise(function (resol, rej) {
          db.query(stmt)
            .then(function (data) {
              resol(data[0].res);
            }, function (err) {
              console.log('Error count assets - ' + err);
              rej(err);
            });
        }))
    }
    Q.all(promises).then(function (result) {
      for (var ind in result) {
        assets += parseInt(result[ind]);
      }
      var result = {status: 200, message: assets};
      resolve(result);
    });
  });
}

assetTable.getPaginatedAssets = function (userId, page, pageSize) {
  var rnmin = pageSize * page + 1;
  var rnmax = pageSize * (page + 1);
  console.log(rnmin);
  console.log(rnmax);
  var stmt = "select * from ((select rownum rn, docs_id as asset_id, my_users_id as user_id, title, posting_date, creation_date, location, dbms_lob.substr(description,32676,1) as description, file_type, asset_path, preview_src_link from docs where my_users_id='" + userId + "' and is_deleted=0"
    + " union "
    + "select rownum rn, photos_id as asset_id, my_users_id as user_id, title, posting_date, creation_date, location, dbms_lob.substr(description,32676,1) as description, file_type, asset_path, preview_src_link from photos where my_users_id='" + userId + "' and is_deleted=0 "
    + " union "
    + "select rownum rn, videos_id as asset_id, my_users_id as user_id, title, posting_date, creation_date, location, dbms_lob.substr(description,32676,1) as description, file_type, asset_path, preview_src_link from videos where my_users_id='" + userId + "' and is_deleted=0"
    + " union "
    + "select rownum rn, pdfs_id as asset_id, my_users_id as user_id, title, posting_date, creation_date, location, dbms_lob.substr(description,32676,1) as description, file_type, asset_path, preview_src_link from pdfs where my_users_id='" + userId + "' and is_deleted=0) ORDER BY POSTING_DATE) "
    + "where rn <= " + rnmax + " and rn >= " + rnmin;
  console.log(stmt);
  return Q.promise(function (resolve, reject) {
    db.query(stmt)
      .then(function (data) {
        resolve(data);
      }, function (Error) {
        console.log("Err getPaginatedAssets: " + JSON.stringify(Error));
        reject(Error);
      })
  })
}

assetTable.getCompleteAsset = function (userId, page, pageSize) {
  var rnminn = pageSize * page + 1;
  var rnmaxx = pageSize * (page + 1);

  console.log(rnminn);
  console.log(rnmaxx);

  var firstTime = 0;
  var stmt = "SELECT * FROM(";
  for (var index in tables) {
    var table = tables[index];
    console.log(table);
    if (firstTime != 0) {
      stmt += " union ";
    } else {
      firstTime = 1;
    }
    stmt += "select rownum rn, " + table + "_id as asset_id, my_users_id as user_id, "
      + "title, TO_CHAR(posting_date,'DD-MON-YY HH:MI:SS') as posting_date, TO_CHAR(creation_date,'DD-MON-YY HH:MI:SS') as creation_date, location,"
      + " dbms_lob.substr(description,32676,1) as description, file_type, "
      + "asset_path, preview_src_link, tags_persons_utils.gettags('"
      + userId + "', " + table + "_id, '" + table.substr(0, table.length - 1)
      + "') as tags, tags_persons_utils.getpeople('" + userId + "', "
      + table + "_id, '" + table.substr(0, table.length - 1)
      + "') as persons from "
      + table + " where my_users_id='" + userId + "' and is_deleted=0 ";
  }
  stmt += "ORDER BY POSTING_DATE) where rn <= " + rnmaxx + " and rn >= " + rnminn;

  console.log(stmt);
  return Q.promise(function (resolve, reject) {
    db.query(stmt)
      .then(function (data) {
        for (var index in data) {
          var tempIndex = index;
          console.log('-------------------------------------');
          console.log(data[tempIndex]);
          console.log('-------------------------------------');
          if (data[tempIndex].tags) {

            data[tempIndex].tags = data[tempIndex].tags.split("SMRP");
            data[tempIndex].tags.splice(data[tempIndex].tags.length - 1);
          }
          if (data[tempIndex].persons) {
            data[tempIndex].persons = data[tempIndex].persons.split("SMRP");
            data[tempIndex].persons.splice(data[tempIndex].persons.length - 1);
          }
        }

        resolve(data);
      }, function (error) {
        console.log("Err getPaginatedAssets: " + JSON.stringify(error));
        reject(error);
      })
  })
}

formatTables = function (tables) {
  var result = [];
  for (var ind in tables) {
    result.push(tables[ind] + 's');
  }
  return result;
}

assetTable.getDBCon = function () {
  return db;
}

assetTable.updateAsset = function (asset) {
  console.log("update " + asset);
  return Q.promise(function (resolve, reject) {
    var table = asset.file_type + 's';
    var stmt = "UPDATE " + table + " SET "
      + "TITLE =" + checkForNULLValues(asset.title) + ", "
      + "POSTING_DATE = TO_TIMESTAMP("
      + checkForNULLValues(asset.posting_date)
      + ",\'DD-MM-YYYY hh:MI:ss\'), "
      + "CREATION_DATE =TO_TIMESTAMP("
      + checkForNULLValues(asset.creation_date)
      + ",\'DD-MM-YYYY hh:MI:ss\'), "
      + "LOCATION=" + checkForNULLValues(asset.location) + ", "
      + "description=" + checkForNULLValues(asset.description)
      + " where " + table + "_ID =" + parseInt(asset.asset_id);
    stmt = stmt.replace(/&/g, "'||utl_url.unescape('%26')||'");
    console.log(stmt);
    db.statement(stmt).then(function (data) {
      console.log('[succes]' + JSON.stringify(data));
      resolve(1);
    }, function (err) {
      console.log('[failure]' + err);
      reject("Problems in updating asset" + err);
    });
  })
}

assetTable.removeAsset = function (asset) {
  console.log('Deleting: ' + JSON.stringify(asset));

  return new Promise(function (resolve, reject) {
    var table = asset.file_type + 's';
    var stmt = "UPDATE " + table + " SET IS_DELETED=1 WHERE " + table + "_ID = " + parseInt(asset.asset_id);
    console.log('[updatestmt]' + stmt);
    db.statement(stmt).then(function (data) {
      console.log('[succes]' + JSON.stringify(data));
      resolve(1);
    }, function (err) {
      console.log('[failure]' + err);
      reject("Problems in updating asset" + err);
    })

  })
}

assetTable.complexSearch = function (userId, object) {
  console.log(userId);
  console.log(object);
  console.log(object.tables);
  if (object.tables.length == 0) {
    object.tables = tables;
  } else {
    object.tables = formatTables(object.tables);
  }
  var promises = [];
  var fields = JSON.parse(JSON.stringify(object));
  delete fields.tables;
  console.log("fields " + JSON.stringify(fields));
  console.log("tables " + JSON.stringify(object.tables));
  var firstTime = 0;
  var stmt = "";
  for (var index in object.tables) {
    var table = object.tables[index];
    console.log(table);
    if (firstTime != 0) {
      stmt += " union ";
    } else {
      firstTime = 1;
    }
    stmt += "select " + table + "_id as asset_id, my_users_id as user_id, "
      + "title, TO_CHAR(posting_date,'DD-MON-YY HH:MI:SS') as posting_date, TO_CHAR(creation_date,'DD-MON-YY HH:MI:SS') as creation_date, location,"
      + " dbms_lob.substr(description,32676,1) as description, file_type, "
      + "asset_path, preview_src_link, tags_persons_utils.gettags('"
      + userId + "', " + table + "_id, '" + table.substr(0, table.length - 1)
      + "') as tags, tags_persons_utils.getpeople('" + userId + "', "
      + table + "_id, '" + table.substr(0, table.length - 1)
      + "') as persons from "
      + table + " where my_users_id='" + userId + "' and is_deleted=0";

    console.log(Object.keys(fields));
    console.log(Object.keys(fields).length);
    if (Object.keys(fields).length != 0) {
      for (var key in fields) {
        stmt += " and UPPER(";
        if (key == "tags") {
          stmt += "tags_persons_utils.gettags('"
            + userId + "', " + table + "_id, '" + table.substr(0, table.length - 1)
            + "')";
        } else if (key == "persons") {
          stmt += "tags_persons_utils.getpeople('" + userId + "', "
            + table + "_id, '" + table.substr(0, table.length - 1)
            + "')";
        } else if (key == "kinship_degree") {
          stmt += "familytree.maximumkinshipofasset('" + userId + "'," + table + "_id, '" + table.substr(0, table.length - 1) + "')"
        } else {
          stmt += key;
        }
        if (key == "kinship_degree") {
          stmt += ") <=" + fields[key];
        } else {
          stmt += ") like '%" + fields[key].toUpperCase() + "%'";
        }

      }
    }

  }
  console.log(stmt);
  return Q.promise(function (resolve, reject) {
    db.query(stmt)
      .then(function (data) {
        for (var index in data) {
          var tempIndex = index;
          console.log(data[tempIndex]);
          // if (data[tempIndex].tags) {
          //     // data[tempIndex].tags = data[tempIndex].tags.split("*SMRP*");
          //     // data[tempIndex].tags.splice(data[tempIndex].tags.length - 1);
          //
          // }
          // if (data[tempIndex].persons) {
          //     data[tempIndex].persons = data[tempIndex].persons.split("*SMRP*");
          //     data[tempIndex].persons.splice(data[tempIndex].persons.length - 1);
          // }
          if (data[tempIndex].tags) {

            data[tempIndex].tags = data[tempIndex].tags.split("SMRP");
            data[tempIndex].tags.splice(data[tempIndex].tags.length - 1);
          }
          if (data[tempIndex].persons) {
            data[tempIndex].persons = data[tempIndex].persons.split("SMRP");
            data[tempIndex].persons.splice(data[tempIndex].persons.length - 1);
          }
        }

        resolve(data);
      }, function (error) {
        console.log("Err getPaginatedAssets: " + JSON.stringify(error));
        reject(error);
      })
  })
}
