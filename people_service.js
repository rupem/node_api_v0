var people = module.exports = {};
var Q = require('q');
var database = require('./db.js');
var db = database.getConnection();


people.getPeople = function(userId, assetId, assetType){
  return Q.promise(function(resolve, reject) {
    if(!assetType){
      var err ={status: 400, message: 'Bad request, type of file was not mentioned'};
      reject(err);
    }
    var stmt = "select * from people where my_users_id =" + userId 
      +" and people_id IN (select people_id from " + assetType + "_people" 
      + " where "+ assetType +"s_id=" + assetId +")";
    console.log(stmt);  
    db.query(stmt)
      .then(function(data){
        resolve(data);
      }, function(err){
        reject(err);
      })  
  })
}