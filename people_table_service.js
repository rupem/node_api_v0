/**
 * Created by RobertHerscovici on 5/30/2017.
 */
var peopleTable = module.exports = {};
var Q = require('q');
var database = require('./db.js');
var db = database.getConnection();

peopleTable.getPeopleId = function (person_nickname, user_id) {
    return new Promise(function (resolve, reject) {
        var selectstmt = "SELECT PEOPLE_ID FROM PEOPLE WHERE MY_USERS_ID = '" + user_id + "' and nickname like '%" + person_nickname + "%'";
        //var selectstmt = "SELECT PEOPLE_ID FROM PEOPLE WHERE MY_USERS_ID = " + Number(user_id);
        console.log(selectstmt);
        db.query(selectstmt).then(function (data) {
            console.log('[data]' + JSON.stringify(data));
            resolve(data[0]);
        }, function (err) {
            console.log('error - ' + err);
            reject(err);
        });

    })

}

peopleTable.insertPersonsIntoPersonsOnly = function (nickname, my_user_id) {
    return new Promise(function (resolve, reject) {
        var insertstmt = "INSERT INTO PEOPLE(NICKNAME,MY_USERS_ID,IS_DELETED) VALUES ('" + nickname + "' , '" + my_user_id + "',0 )";
        console.log(insertstmt);
        db.statement(insertstmt).then(function (response) {
            console.log('[insertPeople] succes' + JSON.stringify(response));
            resolve();
        }, function (err) {
            console.log('[insertPeople] failed' + err);
            reject(err);
        })
    })

}

peopleTable.insertPeopleComplete = function (nickname, my_users_id, asset_id, asset_file_type) {
    console.log('assetId' + asset_id);
    return new Promise(function (resolve, reject) {
        peopleTable.insertPersonsIntoPersonsOnly(nickname, my_users_id).then(function (response) {
            peopleTable.getPeopleId(nickname, my_users_id).then(function (rsp) {
                var insertKeyForeign = "INSERT INTO " + asset_file_type + "_PEOPLE VALUES (10," + Number(rsp.people_id)+ ", " + asset_id  + ",0)";
                console.log('insertKeyForeign ' + insertKeyForeign);
                db.statement(insertKeyForeign).then(function (data) {
                    console.log('[succes]' + JSON.stringify(data));
                    resolve();
                }, function (err) {
                    console.log('[failure]' + err);
                    reject("Problems in insertin into foreign key table" + err);
                })
            }, function (er) {
                reject("Problems getting the people id " + err);

            })
        }, function (err) {
            reject("Problems inserting the people" + err);

        })
    })


}

peopleTable.insertPeopleArray = function (peopleArray) {
    console.log('inside');
    return new Promise(function (resolve, reject) {
        var promiseArray = [];
        for (var i in peopleArray) {
            console.log(JSON.stringify('person in array' + peopleArray[i]));
            promiseArray.push(peopleTable.insertPeopleComplete(peopleArray[i].nickname, peopleArray[i].my_users_id, peopleArray[i].asset_id, peopleArray[i].file_type));
        }
        Promise.all(promiseArray).then(function (data) {
            resolve("OK");
        }, function (err) {
            reject(err);
        })
    })

}

function getAssetId(creation_date, file_type) {
    return new Promise(function (resolve, reject) {
        var selectStmt = "SELECT " + file_type + "_ID AS ASSET_ID FROM " + file_type + " WHERE creation_date = TO_TIMESTAMP( '" + creation_date
            + "',\'DD-MM-YYYY hh:MI:ss\')";

        selectStmt = selectStmt.replace(/&/g, "'||utl_url.unescape('%26')||'");
        console.log('[selectstmt]' + selectStmt);
        db.query(selectStmt).then(function (data) {
            console.log('[data]' + JSON.stringify(data[0]));
            resolve(data);
        }, function (err) {
            console.log('select - ' + err);
            reject(data[0]);
        });

    })

}

